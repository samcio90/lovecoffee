function doubleSlider() {
  $('.left-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,

    responsive: [{
        breakpoint: 1550,
        settings: {
          slidesToShow: 5,

        }
      },
      {
        breakpoint: 1300,
        settings: {
          slidesToShow: 4,

        }
      },
      {
        breakpoint: 1010,
        settings: {
          slidesToShow: 3,

        }
      },
      {
        breakpoint: 785,
        settings: {
          slidesToShow: 2,

        }
      },
      {
        breakpoint: 520,
        settings: {
          slidesToShow: 1,

        }
      },
    ]
  });

}
