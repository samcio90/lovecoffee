function teaSlider() {
  $('.tea-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: $('.tea-prev'),
    nextArrow: $('.tea-next'),

    responsive: [{
        breakpoint: 1550,
        settings: {
          slidesToShow: 3,

        }
      },

      {
        breakpoint: 1250,
        settings: {
          slidesToShow: 2,

        }
      },

      {
        breakpoint: 1030,
        settings: {
          slidesToShow: 1,

        }
      },
      {
        breakpoint: 785,
        settings: {
          slidesToShow: 2,

        }
      },

      {
        breakpoint: 520,
        settings: {
          slidesToShow: 1,

        }
      },

    ]

  });

}
