function coffeeSlider() {
  $('.coffee-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    arrows: true,
    prevArrow: $('.coffee-prev'),
    nextArrow: $('.coffee-next'),

    responsive: [{
        breakpoint: 1550,
        settings: {
          slidesToShow: 2,

        }
      },

      {
        breakpoint: 1250,
        settings: {
          slidesToShow: 1,

        }
      },

      {
        breakpoint: 785,
        settings: {
          slidesToShow: 2,

        }
      },

      {
        breakpoint: 675,
        settings: {
          slidesToShow: 2,

        }
      },

      {
        breakpoint: 520,
        settings: {
          slidesToShow: 1,

        }
      },
    ]
  });

}
